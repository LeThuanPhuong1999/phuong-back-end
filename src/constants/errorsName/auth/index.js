export default {
  NoToken: 'Auth_NoToken',
  AuthorizationFail: 'Auth_Fail',
  ForceLogout: 'Auth_ForceLogout',
  TokenInValid: 'Auth_TokenInValid',
  RoleInValid: 'Role_InValid',
  RoleNotMatch: 'Role_NotMatch'
}