import mongoose from 'mongoose'

const schema = mongoose.Schema

const MD5 = {
    name: 'md5',
    schema: new mongoose.Schema({
        planText: String,
        hasedString: String,
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now }
    })
}

export default mongoose.model('md5', MD5.schema)
