import mongoose from 'mongoose'

var aesSchema = new mongoose.Schema({
    plainText:{
        type:String
    },
    key:{
        type:String
    },
    cipherText:{
        type:String
    }
})

const aesModel = mongoose.model('aes',aesSchema);

export default aesModel