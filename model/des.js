import mongoose from 'mongoose'

var desSchema = new mongoose.Schema({
    plainText:{
        type:String
    },
    key:{
        type:String
    },
    cipherText:{
        type:String
    }
})

const desModel = mongoose.model('des',desSchema);

export default desModel